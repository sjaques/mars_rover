#![allow(dead_code)]

enum Command {
    MoveForward,
    TurnLeft,
    TurnRight
}

#[derive(Copy, Clone, Debug, PartialEq)]
enum Direction {
    North,
    East,
    South,
    West,
}

#[derive(Copy, Clone, Debug, PartialEq)]
struct Position {
    x: i16,
    y: i16,
    d: Direction,
}

trait Drive {
    fn drive(&mut self, commands: Vec<Command>);
}

struct Rover {
    position: Position
}

impl Rover {
    fn get_position(&self) -> &Position { &self.position }
    fn move_forward(&mut self) { self.position.y += 1; }
}

impl Drive for Rover {
    fn drive(&mut self, commands: Vec<Command>) {
        for command in commands {
            match command {
                MoveForward => self.move_forward(),
            }
        }
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_build_rover() {
        let _rover: Rover;
    }

    #[test]
    fn test_start_position_of_rover_is_default_after_build() {
        let start_position = Position{x: 0, y: 0, d: Direction::North};
        let mut rover = Rover{position: start_position};

        assert_eq!(rover.get_position(), &start_position);
    }

    #[test]
    fn test_rover_drive_stands_still_when_no_commands_are_given() {
        let start_position = Position{x: 0, y: 0, d: Direction::North};
        let mut rover = Rover{position: start_position};
        rover.drive(Vec::new());

        assert_eq!(rover.get_position(), &start_position);
    }

    #[test]
    fn test_rover_drive_forward_from_facing_north() {
        let start_position = Position{x: 0, y: 0, d: Direction::North};
        let mut rover = Rover{position: start_position};
        rover.drive(vec![Command::MoveForward]);

        assert_eq!(rover.get_position(), &Position{x: 0, y: 1, d: Direction::North});
    }
}
