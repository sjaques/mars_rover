#include "rover.hpp"

bool operator==(const Position &lhs, const Position &rhs)
{
    return lhs.x == rhs.x && lhs.y == rhs.y && lhs.d == rhs.d;
}

char toChar(Dir direction)
{
    switch (direction)
    {
    case N:
        return 'N';
    case E:
        return 'E';
    case S:
        return 'S';
    case W:
        return 'W';
    default:
        return '/';
    }
}

std::ostream &operator<<(std::ostream &os, const Position &pos)
{
    os << pos.x << ":" << pos.y << ":" << toChar(pos.d);
    return os;
}

Rover::Rover(const Position &startPosition) : _position{startPosition}
{
}

Position Rover::drive(const std::string &movements)
{
    for (char m : movements)
    {
        switch (m)
        {
        case 'M':
            moveForward();
            break;
        case 'L':
            turnLeft();
            break;
        case 'R':
            turnRight();
            break;
        default:
            return {};
        }
    }
    return _position;
}

void Rover::turnLeft()
{
    _position.d = static_cast<Dir>((static_cast<std::int8_t>(_position.d) + 3) % 4);
}

void Rover::turnRight()
{
    _position.d = static_cast<Dir>((static_cast<std::int8_t>(_position.d) + 1) % 4);
}

void Rover::moveForward()
{
    switch (_position.d)
    {
    case N:
        ++_position.y;
        break;
    case E:
        ++_position.x;
        break;
    case S:
        --_position.y;
        break;
    case W:
        --_position.x;
        break;
    default:
        return;
    }
}
