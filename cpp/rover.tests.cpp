#include "rover.hpp"

#include <gmock/gmock.h>

#include <string>

using namespace ::testing;

int main(int argc, char **argv)
{
    testing::InitGoogleMock(&argc, argv);
    return RUN_ALL_TESTS();
}

TEST(Rover, create_rover)
{
    Position startPosition{};

    Rover rover(startPosition);
}

TEST(Rover, stand_still_gives_same_end_as_start_position)
{
    Position startPosition{1, 2, Dir::N};
    std::string noMovements;

    Rover rover(startPosition);

    EXPECT_THAT(rover.drive(noMovements), Eq(Position{1, 2, Dir::N}));
}

TEST(Rover, turn_right_once)
{
    Position startPosition{3, 5, Dir::N};
    std::string movements{"R"};

    Rover rover(startPosition);

    EXPECT_THAT(rover.drive(movements), Eq(Position{3, 5, Dir::E}));
}

TEST(Rover, turn_left_once)
{
    Position startPosition{8, 6, Dir::N};
    std::string movements{"L"};

    Rover rover(startPosition);

    EXPECT_THAT(rover.drive(movements), Eq(Position{8, 6, Dir::W}));
}

TEST(Rover, turn_left_4_times_does_not_change_direction)
{
    Position startPosition{8, 6, Dir::N};
    std::string movements{"LLLL"};

    Rover rover(startPosition);

    EXPECT_THAT(rover.drive(movements), Eq(startPosition));
}

TEST(Rover, turn_right_4_times_does_not_change_direction)
{
    Position startPosition{8, 6, Dir::N};
    std::string movements{"RRRR"};

    Rover rover(startPosition);

    EXPECT_THAT(rover.drive(movements), Eq(startPosition));
}

TEST(Rover, facing_north_move_forward)
{
    Position startPosition{8, 6, Dir::N};
    std::string movements{"M"};

    Rover rover(startPosition);

    EXPECT_THAT(rover.drive(movements), Eq(Position{8, 7, Dir::N}));
}

TEST(Rover, facing_north_move_3_times_forward)
{
    Position startPosition{8, 3, Dir::N};
    std::string movements{"MMM"};

    Rover rover(startPosition);

    EXPECT_THAT(rover.drive(movements), Eq(Position{8, 6, Dir::N}));
}

TEST(Rover, facing_north_move_forward_and_turn_right)
{
    Position startPosition{8, 3, Dir::N};
    std::string movements{"MR"};

    Rover rover(startPosition);

    EXPECT_THAT(rover.drive(movements), Eq(Position{8, 4, Dir::E}));
}

TEST(Rover, facing_north_move_forward_and_turn_left)
{
    Position startPosition{8, 3, Dir::N};
    std::string movements{"ML"};

    Rover rover(startPosition);

    EXPECT_THAT(rover.drive(movements), Eq(Position{8, 4, Dir::W}));
}

TEST(Rover, facing_north_move_forward_and_turn_left_twice)
{
    Position startPosition{8, 3, Dir::N};
    std::string movements{"MLL"};

    Rover rover(startPosition);

    EXPECT_THAT(rover.drive(movements), Eq(Position{8, 4, Dir::S}));
}

TEST(Rover, facing_east_move_forward)
{
    Position startPosition{1, 2, Dir::E};
    std::string movements{"M"};

    Rover rover(startPosition);

    EXPECT_THAT(rover.drive(movements), Eq(Position{2, 2, Dir::E}));
}

TEST(Rover, facing_south_move_forward)
{
    Position startPosition{1, 2, Dir::S};
    std::string movements{"M"};

    Rover rover(startPosition);

    EXPECT_THAT(rover.drive(movements), Eq(Position{1, 1, Dir::S}));
}

TEST(Rover, facing_west_move_forward)
{
    Position startPosition{1, 2, Dir::W};
    std::string movements{"M"};

    Rover rover(startPosition);

    EXPECT_THAT(rover.drive(movements), Eq(Position{0, 2, Dir::W}));
}

TEST(Rover, challenge_1_from_assignment_without_grid)
{
    Position startPosition{0, 0, Dir::N};
    std::string movements{"MMRMMLM"};

    Rover rover(startPosition);

    EXPECT_THAT(rover.drive(movements), Eq(Position{2, 3, N}));
}

TEST(Rover, DISABLED_challenge_2_from_assignment_with_grid)
{
    Position startPosition{0, 0, Dir::N};
    std::string movements{"MMMMMMMMMM"};

    Rover rover(startPosition);

    EXPECT_THAT(rover.drive(movements), Eq(Position{0, 0, N}));
}

TEST(Rover, DISABLED_challenge_3_from_assignment_with_obstacle)
{
    Position startPosition{0, 0, Dir::N};
    //Obstacle obstacle{0, 3};
    std::string movements{"MMMM"};

    Rover rover(startPosition);

    //EXPECT_THAT(rover.drive(movements), Eq({Position{0, 2, N}, true}));
}
