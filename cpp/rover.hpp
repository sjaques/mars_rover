#ifndef ROVER_INCLUDED
#define ROVER_INCLUDED

#include <string>
#include <cstdint>
#include <iostream>

enum Mov
{
    L,
    R,
    M,
};

enum Dir : std::int8_t
{
    N = 0,
    E,
    S,
    W,
};
char toChar(Dir direction);

struct Position
{
    int x{};
    int y{};
    Dir d{Dir::N};
};
bool operator==(const Position &lhs, const Position &rhs);
std::ostream &operator<<(std::ostream &os, const Position &pos);

class Rover
{
public:
    Rover(const Position &startPosition);
    Position drive(const std::string &movements);

private:
    void turnLeft();
    void turnRight();
    void moveForward();

    Position _position;
};

#endif
